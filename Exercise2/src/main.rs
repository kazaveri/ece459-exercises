// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 1 || n == 2{
        return 1;
    }
    let mut n2 = 1;
    let mut n1 = 1;
    let mut ret = 1;
    let mut i = 2;
    while i < n{
        let tmp = ret;
        ret = n2 + n1;
        n2 = n1;
        n1 = tmp;
        i = i + 1;
    }
    ret
}


fn main() {
    println!("{}", fibonacci_number(1));
    println!("{}", fibonacci_number(2));
    println!("{}", fibonacci_number(3));
    println!("{}", fibonacci_number(4));
    println!("{}", fibonacci_number(5));
    println!("{}", fibonacci_number(10));
}
