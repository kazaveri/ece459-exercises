use std::thread;

static N: i32 = 10;  // number of threads to spawn

//see https://doc.rust-lang.org/rust-by-example/std_misc/threads.html

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];
    for i in 0..N {
        children.push(thread::spawn(move || {
            println!("this is thread number {}", i);
        }));
    }
    for child in children{
        let _ = child.join();
    }
}
