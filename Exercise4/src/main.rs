use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N : i32 = 10;
// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let mut children = vec![];
    for i in 0..N{

        let tx1 = mpsc::Sender::clone(&tx);
        let child = (thread::spawn(move || {
            tx1.send(i).unwrap();
            thread::sleep(Duration::from_millis(1000));
        }));
        children.push(child);
    }
    
    for received in rx {
        println!("Got: {}", received);
    }

    for child in children {
        let _ = child.join().unwrap();
    }
}
